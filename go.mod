module gitee.com/eden-framework/plugin-cache

go 1.16

require (
	gitee.com/eden-framework/enumeration v1.0.1
	gitee.com/eden-framework/plugin-redis v0.1.3
	gitee.com/eden-framework/plugins v0.0.7
	github.com/cornelk/hashmap v1.0.1
	github.com/profzone/envconfig v1.4.6
)
